\beamer@endinputifotherversion {3.36pt}
\select@language {italian}
\select@language {italian}
\beamer@sectionintoc {1}{Runaway electrons}{3}{0}{1}
\beamer@sectionintoc {2}{Algoritmo ML-EM}{6}{0}{2}
\beamer@sectionintoc {3}{Ricostruzione di immagini PET}{8}{0}{3}
\beamer@sectionintoc {4}{Convalida codice}{9}{0}{4}
\beamer@sectionintoc {5}{Spettri AUG}{11}{0}{5}
\beamer@sectionintoc {6}{Conclusioni}{17}{0}{6}
