# Presentazione 20/03/2018

##  Intro
Elenco quanto svolto in tesi

## Algoritmo
Spiego l'algoritmo base senza parlare delle estensioni dell'algoritmo

## Test:
### Slide 1
Picchi gaussiani stretti -> delta di dirac
Cosa sono gli spettri sintetici
Perche' DRF gaussiana

### Slide 2
L'algoritmo converge alla distribuzione di partenza (chi quadro conferma)
Fatica nelle zone dove la derivata prima varia notevolmente
Nei picchi stretti uso boost

### Slide 3
Non convergo piu'
Devo usare lo smoothing

## Convalida
### Slide 1
Come ho fatto a ricostruire la DRF
Caratteristiche DRF

### Slide 2
Ricostruzione ben riuscita (ma no info su intensita' picchi)
Potrebbe essere migliorata aggiungendo fisica nella DRF
Se anche aggiungo errori sistematici nella DRF la deconvoluzione non cambia 
molto

## Runaway
### Slide 1
L'algoritmo fallisce nel deconvolvere anche spettri sintetici

### Slide 2-3
Occorre correggere la formula per la stima dello spettro sorgente:
Normalizzo la formula per la probabilita' di misurare un fotone gamma generato
da un elettrone runaway con energia x_i.

### Slide 4
Riesco a deconvolvere bene lo spettro
Ci sono alcuni artifatti dovuti alla forma della DRF

### Slide 5a
Spiego come si generano e si mitigano i RE su AUG
Entrambe le tecniche di mitigazione sono accompagnate da una diminuzione della
corrente, voglio vedere se attuano anche una:
**ingegneria dello spazio delle fasi degli elettroni runaway** 
ossia se modificano la loro distribuzione delle velocita'.

### Slide 5
Evoluzione della popolazione RE durante una scarica
Forse e' dovuta al pile up di qualche evento nel primo spettro

### Slide 6
Confronto scarica con e senza RMPs