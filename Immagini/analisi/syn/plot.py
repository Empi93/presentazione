#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May  4 22:00:06 2017

@author: enrico

	Plot the measured spectrum and the convolved spectrum!
    
    python plot.py energy.dat conv.dat spectrum.dat en_teo.dat errors.dat
"""

import matplotlib.pyplot as plt
import scipy
import numpy as np
import math
import sys
import string

###   READ FILE   ###
x_axis, x_deco, err_deco = np.loadtxt(sys.argv[5], unpack=True)
x_axis, x_deco = np.loadtxt(sys.argv[1], unpack=True)
x_teo = np.loadtxt(sys.argv[4], unpack=True)

err_deco = err_deco * sum(x_teo) / sum(x_deco)
x_deco = x_deco * sum(x_teo) / sum(x_deco)
#x_teo = x_teo * sum(x_deco)/sum(x_teo)

y_spectrum = np.loadtxt(sys.argv[3], unpack=True)
x_axis, y_conv =  np.loadtxt(sys.argv[2], unpack=True)

###   PLOT   ###
plt.close('all')

plt.figure(facecolor='white')

plt.title("34084 (1.000-1.130 s) reconstruction (5)", fontsize=18)
plt.xlabel("energy [MeV]", fontsize=12)
plt.ylabel("counts", fontsize=12)

axes = plt.gca()
axes.set_xlim([5, 15])

plt.plot(x_axis,x_deco+err_deco, linestyle='--', color='dodgerblue', linewidth=1.2)
plt.plot(x_axis,x_deco-err_deco, linestyle='--', color='dodgerblue', linewidth=1.2)
plt.plot(x_axis,x_deco, 'dodgerblue', linewidth=2.2, label= "smooth 5")
plt.plot(x_axis,x_teo, 'darksalmon', linewidth=2.2, label= "source distribution")

plt.legend(loc=1, fontsize=10)
plt.savefig('gauss_10_5_spettro_deconvoluto.pdf', format='pdf')
plt.show()
plt.show()

###   LOG PLOT   ###
plt.figure(facecolor='white')

plt.title("34084 (1.000-1.130 s) measured", fontsize=18)
plt.xlabel("energy [MeV]", fontsize=12)
plt.ylabel("counts", fontsize=12)
plt.yscale('log')
axes = plt.gca()
axes.set_xlim([0, 11])
axes.set_ylim([0.6, 5000])


plt.plot(x_axis,y_spectrum, 'or', markersize=3, label= "synthetic spectrum")
plt.plot(x_axis,y_conv, 'seagreen', linewidth=2.2, label= "riconvoluted spectrum")

plt.legend(loc=1, fontsize=10)
plt.savefig('gauss_10_5_spettro_misurato_log.pdf', format='pdf')
plt.show()
plt.show()