#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May  4 22:00:06 2017

@author: enrico

	Plot the measured spectrum and the convolved spectrum!
    
    python confronto_plot.py energy.dat conv.dat spectrum.dat en_teo.dat test3
"""

import matplotlib.pyplot as plt
import scipy
import numpy as np
import math
import sys
import string


###   READ FILE   ###
in_fp = file("test4_d_1_20/energy.dat", 'r')
all_file = in_fp.readlines()
in_fp.close()

x = np.zeros(len(all_file))
en1 = np.zeros(len(all_file)) #energy

for i in range(len(all_file)):
    #if (i == 0):   # header
        #continue
    elements = string.split(all_file[i])
    x[i] = float(elements[0])
    en1[i] = float(elements[1])

in_fp = file("test4_d_1_21/energy.dat", 'r')
all_file = in_fp.readlines()
in_fp.close()

en2 = np.zeros(len(all_file)) #energy

for i in range(len(all_file)):
    #if (i == 0):   # header
        #continue
    elements = string.split(all_file[i])
    x[i] = float(elements[0])
    en2[i] = float(elements[1])
    

in_fp = file("test4_d_1_22/energy.dat", 'r')
all_file = in_fp.readlines()
in_fp.close()

en3 = np.zeros(len(all_file)) #energy

for i in range(len(all_file)):
    #if (i == 0):   # header
        #continue
    elements = string.split(all_file[i])
    x[i] = float(elements[0])
    en3[i] = float(elements[1])
    
in_fp = file("test4_d_1_23/energy.dat", 'r')
all_file = in_fp.readlines()
in_fp.close()

en4 = np.zeros(len(all_file)) #energy

for i in range(len(all_file)):
    #if (i == 0):   # header
        #continue
    elements = string.split(all_file[i])
    x[i] = float(elements[0])
    en4[i] = float(elements[1])

in_fp = file("test4_d_1_24/energy.dat", 'r')
all_file = in_fp.readlines()
in_fp.close()

en5 = np.zeros(len(all_file)) #energy

for i in range(len(all_file)):
    #if (i == 0):   # header
        #continue
    elements = string.split(all_file[i])
    x[i] = float(elements[0])
    en5[i] = float(elements[1])

in_fp = file("test4_d_1_25/energy.dat", 'r')
all_file = in_fp.readlines()
in_fp.close()

en6 = np.zeros(len(all_file)) #energy

for i in range(len(all_file)):
    #if (i == 0):   # header
        #continue
    elements = string.split(all_file[i])
    x[i] = float(elements[0])
    en6[i] = float(elements[1])
    
###   PLOT ALL   ###
plt.close('all')

plt.figure(facecolor='white')

plt.title("Deconvolved spectra", fontsize=18)
plt.xlabel("energy", fontsize=12)
plt.ylabel("counts", fontsize=12)

plt.plot(x,en1, 'r', linewidth=1.1)
plt.plot(x,en2, 'b', linewidth=1.1)
plt.plot(x,en3, 'g', linewidth=1.1)
plt.plot(x,en4, 'k', linewidth=1.1)
plt.plot(x,en5, 'k', linewidth=1.1)
plt.plot(x,en6, 'k', linewidth=1.1)
plt.savefig('all_energies.pdf', format='pdf')
plt.show()

energy = np.zeros(len(all_file)) #energy

energy = (en1 + en2 + en3 + en4 + en5 + en6)/5

###   PLOT MEAN   ###
plt.figure(facecolor='white')

plt.title("Mean deconvolved spectrum", fontsize=18)
plt.xlabel("energy", fontsize=12)
plt.ylabel("counts", fontsize=12)

plt.plot(x,energy, 'r', linewidth=1.1)
plt.savefig('mean_energy.pdf', format='pdf')
plt.show()

###   PLOT   ###
plt.figure(facecolor='white')

plt.title("Mean deconvolved spectrum", fontsize=18)
plt.xlabel("energy", fontsize=12)
plt.ylabel("counts", fontsize=12)

plt.plot(x,en1, 'oc', linewidth=0.2)
plt.plot(x,en2, 'ob', linewidth=0.2)
plt.plot(x,en3, 'og', linewidth=0.2)
plt.plot(x,en4, 'ok', linewidth=0.2)
plt.plot(x,en5, 'oy', linewidth=1.1)
plt.plot(x,en6, 'om', linewidth=1.1)
plt.plot(x,energy, 'm', linewidth=2.)
plt.savefig('confronto.pdf', format='pdf')
plt.show()