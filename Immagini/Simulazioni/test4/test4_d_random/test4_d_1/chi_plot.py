#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May 10 13:32:34 2017

@author: enrico

	Plot the various chi square I've generated!
    
    python chi_plot.py precision1.dat precision2.dat precision3.dat likelihood.dat test3
"""

import matplotlib.pyplot as plt
import scipy
import numpy as np
import math
import sys
import string


###   READ FILE   ###
chi1_file =  sys.argv[1]
in_fp = file(chi1_file, 'r')
all_file = in_fp.readlines()
in_fp.close()

iteration = np.zeros(len(all_file))
chi1 = np.zeros(len(all_file)) 

for i in range(len(all_file)):
    elements = string.split(all_file[i])
    iteration[i] = float(elements[0])
    chi1[i] = float(elements[1])

chi2_file =  sys.argv[2]
in_fp = file(chi2_file, 'r')
all_file = in_fp.readlines()
in_fp.close()

chi2 = np.zeros(len(all_file)) 

for i in range(len(all_file)):
    elements = string.split(all_file[i])
    iteration[i] = float(elements[0])
    chi2[i] = float(elements[1])

chi3_file =  sys.argv[3]
in_fp = file(chi3_file, 'r')
all_file = in_fp.readlines()
in_fp.close()

chi3 = np.zeros(len(all_file))

for i in range(len(all_file)):
    elements = string.split(all_file[i])
    iteration[i] = float(elements[0])
    chi3[i] = float(elements[1])
    

like_file =  sys.argv[4]
in_fp = file(like_file, 'r')
all_file = in_fp.readlines()
in_fp.close()

likelihood = np.zeros(len(all_file))

for i in range(len(all_file)):
    elements = string.split(all_file[i])
    iteration[i] = float(elements[0])
    likelihood[i] = float(elements[1])

###   PLOT   ###
plt.close('all')
plt.figure(facecolor='white')
plt.figure(1)

plt.subplot(411)
plt.title("Chi Squares "+sys.argv[4], fontsize=18)
plt.plot(iteration,chi1, 'or', linewidth=1.1, label= "norms")
plt.legend(loc=1, fontsize=10)
plt.ylabel("norm difference", fontsize=12)

plt.subplot(412)
plt.plot(iteration,chi2, 'or', linewidth=1.1, label= "chi spectrum")
plt.legend(loc=1, fontsize=10)
plt.ylabel("chi", fontsize=12)

plt.subplot(413)
plt.plot(iteration,chi3, 'or', linewidth=1.1, label= "chi energy")
plt.legend(loc=1, fontsize=10)
plt.ylabel("chi", fontsize=12)

plt.subplot(414)
plt.plot(iteration,likelihood, 'or', linewidth=1.1, label= "likelihood")
plt.legend(loc=1, fontsize=10)
plt.xlabel("iterations", fontsize=12)
plt.ylabel("likelihood", fontsize=12)

plt.savefig('chi.pdf', format='pdf')
plt.show()

#plt.plot(ecpf[5].t,ecpf[5].v, 'ro')
plt.show()