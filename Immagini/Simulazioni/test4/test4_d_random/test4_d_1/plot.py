""" 
	Plot Results
    
    python plot.py path_to_file graph_title x_label y_label
"""

import matplotlib.pyplot as plt
import scipy
import numpy as np
import math
import sys
import string


###   READ FILE   ###
name =  sys.argv[1]
in_fp = file(name, 'r')
all_file = in_fp.readlines()
in_fp.close()

x = np.zeros(len(all_file))
y = np.zeros(len(all_file))

for i in range(len(all_file)):
    #if (i == 0):   # header
        #continue
    elements = string.split(all_file[i])
    x[i] = float(elements[0])
    y[i] = float(elements[1])
  
    
###   PLOT   ###
plt.close('all')

plt.figure(facecolor='white')

plt.title(sys.argv[2], fontsize=18)
plt.xlabel(sys.argv[3], fontsize=12)
plt.ylabel(sys.argv[4], fontsize=12)

plt.plot(x,y, 'b', linewidth=1.1, label= "deconvoluted energy")
# 'ob' = blu points
#plt.errorbar(xdata, ydata, yerr=yerr, fmt='.', \
#			 color='r', mew = 2, label="segnali sniffer")
plt.legend( loc=1, fontsize=10)
plt.savefig(name[0:-4]+'.pdf', format='pdf')
plt.show()




#plt.plot(ecpf[5].t,ecpf[5].v, 'ro')
plt.show()