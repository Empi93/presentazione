"""
    Program to generate syntetic spectra
    
    TEST 2
    2 gaussians
"""
import matplotlib.pyplot as plt
import scipy
import numpy as np
import math


'''   Detector Response Function   '''
def f_gauss(E, sigma, Ei):
    return 1./(np.sqrt(2.*np.pi)*sigma)*np.exp(-(E-Ei)**2/(2.*sigma**2))

def f_sigma(Ei):
    return 0.05*np.sqrt(662.)**3/np.sqrt(Ei)


'''    MAIN    '''
E_min = -10.         # minimum energy of the spectra (KeV)
E_max = 10.        # max energy of the spectra (KeV)
Ei1 = -4.            # energy of gamma beam (KeV)
A1 = 1000#700000          # number of photons, minumum numbers to make an analysis
Ab = 10000
bins = 300
step = (E_max-E_min)/bins           # bin width (KeV)

sigma1 = 0.5   #f_sigma(Ei1)
x_axis = np.arange(E_min,E_max,step)
x_try = np.random.normal(Ei1, sigma1, A1)
x_try_bg=np.random.exponential(5., Ab)-10.
x, temp = np.histogram(x_try, bins, (E_min, E_max))
x_bg, temp = np.histogram(x_try_bg, bins, (E_min, E_max))
x = x+x_bg
# x = A1*f_gauss(x_axis, sigma1, Ei1) #+A2*f_gauss(x_axis, sigma2, Ei2)

v_Ei = np.arange(E_min,E_max+step,step)

out_fp = file('drf.dat', 'w')
h = np.zeros((len(x_axis), len(x_axis)))

for i in range(len(x_axis)):
    h[i] = A1*f_gauss(x_axis, sigma1, v_Ei[i])
    for j in range(len(h[i])):
        out_fp.write(str(h[i][j]) + '    ')
    out_fp.write('\n')
    
out_fp.close()

y = np.zeros(len(x))
y = h.dot(x)
#for i in range(len(x_axis)):
#    for j in range(len(x_axis)):
#        y[i]+=h[i][j]*x[j]

y = y*np.sum(x)/np.sum(y)

out_fp = file('spectrum.dat', 'w')

# np.savetxt('test.out', (x_axis, y_axis)) 
for i in range(len(x_axis)):
    # out_fp.write(str(x_axis[i]) + '    ' + str(y_axis[i]) + '\n')
    out_fp.write(str(y[i]) + '\n')
        
out_fp.close()



out_fp = file('en_teo.dat', 'w')

# np.savetxt('test.out', (x_axis, y_axis)) 
for i in range(len(x_axis)):
    # out_fp.write(str(x_axis[i]) + '    ' + str(y_axis[i]) + '\n')
    out_fp.write(str(x[i]) + '\n')
        
out_fp.close()


plt.figure(facecolor='white')

plt.xlabel("energy", fontsize=12)
plt.ylabel("counts", fontsize=12)

plt.plot(x_axis,y, 'r', linewidth=1.1, label= "measured spectrum")
plt.plot(x_axis,x, 'b', linewidth=1.1, label= "synthetic spectrym")
plt.legend(loc=1, fontsize=10)
plt.savefig('confronto.pdf', format='pdf')
plt.show()

#plt.plot(ecpf[5].t,ecpf[5].v, 'ro')
plt.show()