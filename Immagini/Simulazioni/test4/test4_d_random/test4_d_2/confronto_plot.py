#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May  4 22:00:06 2017

@author: enrico

	Plot the measured spectrum and the convolved spectrum!
    
    python confronto_plot.py energy.dat conv.dat spectrum.dat en_teo.dat test3
"""

import matplotlib.pyplot as plt
import scipy
import numpy as np
import math
import sys
import string


###   READ FILE   ###
f_energy =  sys.argv[1]
in_fp = file(f_energy, 'r')
all_file = in_fp.readlines()
in_fp.close()

x = np.zeros(len(all_file))
y = np.zeros(len(all_file)) #energy

norm_y =0.
for i in range(len(all_file)):
    #if (i == 0):   # header
        #continue
    elements = string.split(all_file[i])
    x[i] = float(elements[0])
    y[i] = float(elements[1])
    norm_y += y[i]

f_spectrum =  sys.argv[3]
in_fp = file(f_spectrum, 'r')
z = in_fp.readlines() #spectrum
in_fp.close()

f_eteo =  sys.argv[4]
in_fp = file(f_eteo, 'r')
en_teo = in_fp.readlines() #spectrum
in_fp.close()
    
f_conv =  sys.argv[2]
in_fp = file(f_conv, 'r')
all_file = in_fp.readlines()
in_fp.close()

h = np.zeros(len(all_file)) #convoluted
norm_h = 0.
norm_z = 0.

for i in range(len(all_file)):
    #if (i == 0):   # header
        #continue
    elements = string.split(all_file[i])
    x[i] = float(elements[0])
    h[i] = float(elements[1])
    norm_h += h[i]
    z[i] = float(z[i])
    en_teo[i] = float(en_teo[i])
    norm_z += z[i]

print (norm_y)
print (norm_z)
print (norm_h)

for i in range(len(all_file)):
    h[i] = h[i]*norm_z/norm_h
    
norm_h = 0.
for i in range(len(all_file)):
    norm_h += h[i]
    
print (norm_h)



###   PLOT   ###
plt.close('all')

plt.figure(facecolor='white')

plt.title(sys.argv[5], fontsize=18)
plt.xlabel("energy", fontsize=12)
plt.ylabel("counts", fontsize=12)

plt.plot(x,z, 'r', linewidth=1.1, label= "measured spectrum")
plt.plot(x,en_teo, 'b', linewidth=1.1, label= "synthetic spectrym")
plt.plot(x,h, 'og', linewidth=1.1, label= "convoluted spectrum")
plt.plot(x,y, 'or', linewidth=1.1, label= "deconvoluted spectrum")
plt.legend(loc=1, fontsize=10)
plt.savefig('confronto.pdf', format='pdf')
plt.show()

#plt.plot(ecpf[5].t,ecpf[5].v, 'ro')
plt.show()