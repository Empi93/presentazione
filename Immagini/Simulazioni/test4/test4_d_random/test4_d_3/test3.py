"""
    Program to generate syntetic spectra
    
    TEST 2
    2 gaussians
"""
import matplotlib.pyplot as plt
import scipy
import numpy as np
import math


'''   Detector Response Function   '''
def f_gauss(E, sigma, Ei):
    return 1./(np.sqrt(2.*np.pi)*sigma)*np.exp(-(E-Ei)**2/(2.*sigma**2))

def f_sigma(Ei):
    return 0.05*np.sqrt(662.)**3/np.sqrt(Ei)


'''    MAIN    '''
E_min = -5.         # minimum energy of the spectra (KeV)
E_max = 5.        # max energy of the spectra (KeV)
step = .06            # bin width (KeV)
Ei1 = 0.            # energy of gamma beam (KeV)
A1 = 1.#700000           # number of photons, minumum numbers to make an analysis

sigma1 = 1./np.sqrt(2.)
x_axis = np.arange(E_min,E_max+step,step)
x = A1*f_gauss(x_axis, sigma1, Ei1) #+A2*f_gauss(x_axis, sigma2, Ei2)

v_Ei = np.arange(E_min,E_max+step,step)

out_fp = file('drf.dat', 'w')
h = np.zeros((len(x_axis), len(x_axis)))

for i in range(len(h)):
    h[i] = A1*f_gauss(x_axis, sigma1, v_Ei[i])
    for j in range(len(h[i])):
        out_fp.write(str(h[i][j]) + '    ')
    out_fp.write('\n')
    
out_fp.close()

y = np.zeros(len(x))
y = h.dot(x)

out_fp = file('spectrum.dat', 'w')

# np.savetxt('test.out', (x_axis, y_axis)) 
for i in range(len(x_axis)):
    # out_fp.write(str(x_axis[i]) + '    ' + str(y_axis[i]) + '\n')
    out_fp.write(str(y[i]) + '\n')
        
out_fp.close()

x = x*np.sum(y)/np.sum(x)

out_fp = file('en_teo.dat', 'w')

# np.savetxt('test.out', (x_axis, y_axis)) 
for i in range(len(x_axis)):
    # out_fp.write(str(x_axis[i]) + '    ' + str(y_axis[i]) + '\n')
    out_fp.write(str(x[i]) + '\n')
        
out_fp.close()